<?php
namespace Drupal\syslog_advanced\Logger;

use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
//use Psr\Log\LoggerInterface;

class systemlogAdvancedAutoAdd implements LoggerInterface {
  use RfcLoggerTrait;

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = array()) {
    //dsm(LOG_LOCAL0);
    global $base_url;
    $type = ($context['channel']);
    _syslog_advanced_init_log();
    _syslog_advanced_init_log();
    require_once DRUPAL_ROOT . '/core/includes/common.inc';
    // Check the severity filter to see if this message is allowed. We must load
    // common.inc before calling functions from it, since hook_watchdog() can be
    // invoked early in the bootstrap._
    _syslog_advanced_autoadd($type);
    if (_syslog_advanced_is_allowed($type, $level)) {
      $message = strtr( '!base_url|!timestamp|!type|!ip|!request_uri|!referer|!uid|!link|!message', array(
        '!base_url' => $base_url,
        '!timestamp' => $context['timestamp'],
        '!type' => $context['%type'],
        '!ip' => $context['ip'],
        '!request_uri' => $context['request_uri'],
        '!referer' => $context['referer'],
        '!uid' => $context['uid'],
        '!link' => strip_tags($context['link']),
        '!message' => strip_tags(!isset($context['variables']) ? $context['!message'] : strtr($context['message'], $context['variables'])),
   ));
     syslog($level, $message);
    }
  }
}
